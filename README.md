<!-- These are examples of badges you might want to add to your README:
     please update the URLs accordingly

[![Built Status](https://api.cirrus-ci.com/github/<USER>/recorder.svg?branch=main)](https://cirrus-ci.com/github/<USER>/recorder)
[![ReadTheDocs](https://readthedocs.org/projects/recorder/badge/?version=latest)](https://recorder.readthedocs.io/en/stable/)
[![Coveralls](https://img.shields.io/coveralls/github/<USER>/recorder/main.svg)](https://coveralls.io/r/<USER>/recorder)
[![PyPI-Server](https://img.shields.io/pypi/v/recorder.svg)](https://pypi.org/project/recorder/)
[![Conda-Forge](https://img.shields.io/conda/vn/conda-forge/recorder.svg)](https://anaconda.org/conda-forge/recorder)
[![Monthly Downloads](https://pepy.tech/badge/recorder/month)](https://pepy.tech/project/recorder)
[![Twitter](https://img.shields.io/twitter/url/http/shields.io.svg?style=social&label=Twitter)](https://twitter.com/recorder)
-->

[![Project generated with PyScaffold](https://img.shields.io/badge/-PyScaffold-005CA0?logo=pyscaffold)](https://pyscaffold.org/)

# recorder

> Record and interact with super auto pets

A longer description of your project goes here...


<!-- pyscaffold-notes -->

## Note

This project has been set up using PyScaffold 4.4. For details and usage
information on PyScaffold see https://pyscaffold.org/.
